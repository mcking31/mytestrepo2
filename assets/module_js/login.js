(function($) {
    $(".show-password").click(function(){
        $(this).parents(".password-form").find(".field").attr('type', function(index, attr){
            return attr == 'password' ? 'text' : 'password';
        });

        $(this).text( function(index, text){
            return text == 'Show' ? 'Hide' : 'Show';
        });

        $(this).attr('Title', function(index, attr){
            return attr == 'Show Password' ? 'Mask Password' : 'Show Password';
        });
    });

    $(document).ready(function(e) {
        $('#login').click(function() {

            var sEmail = $('#email').val();
			var $btn = $(this);
			
			$.post(
				baseurl + 'login/verify',
				{
					'username' : $('#email').val(),
					'password' : $('#password').val()
				}
			).done(function(data){
			
				if( !data.response ){
					var currentAttemptCount = $("#attempts").val();
					var attemptAdd = parseInt(currentAttemptCount) + 1;

					if(attemptAdd == 1){
						$(".login-form").find(".error-message").html("Invalid username or password. Please try again.");
					}

					if(attemptAdd == 2){
						$(".login-form").find(".error-message").html("Warning. Entering another invalid password will temporarily disable your account.");
					}

					if(attemptAdd == 3){
						$(".login-form").find(".error-message").html("Your account has been temporarily disabled. Please call TrueMoney at (02) 718-9999.");
					}

					$("#attempts").val(attemptAdd);
					
					$(".login-form").find(".error-message").html(data.message);
					$btn.html('Login').removeClass('disabled');
				}

				else {
					window.location.href = baseurl + 'dashboard';
				}
			
			});
        });

        $('#validateEmail').click(function() {
            var dataLoadingLabel = $(this).attr("data-loading-label");

            var sEmail = $('#email').val();
            if ($.trim(sEmail).length == 0) {
                $(".login-form").find(".error-message").html("Please enter a valid email address.");
                $("#email").parents(".form").addClass("error");
                e.preventDefault();
            }

            if (validateEmail(sEmail)) {
                $(".login-form").find(".error-message").html("");
                $("#email").parents(".form").removeClass("error");
                $(this).addClass("disabled").html("<div class='ui active tiny inline loader inverted'></div> "+dataLoadingLabel);
            }
            else {
                $(".login-form").find(".error-message").html("Invalid email address. Please try again.");
                $("#email").parents(".form").addClass("error");
                e.preventDefault();
            }
        });
    });

    function validateEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    }

    $("#proceedDashboard").on("click", function(){
        window.location.replace("index.html");
    });

    $(".modal-trigger").on("click", function(){
        var modal = $(this).attr("data-modal");
        $(modal).modal({
            duration: "50"
        }).modal("show");
    });

    $(".ui.modal .close").on("click", function(){
        $(this).parents(".modal").modal("hide");
    });

    $(".new-password").on("keyup", function(){
        var passStrength = $(".pass-hint").text();
        var newPassword = $(this).val();
        if((passStrength == "Too Short") || (passStrength == "Weak") || (passStrength == "Very Weak")){
            $(".pass-hint").css({
                "color" : "#ec1d24"
            });
        }
        else if(passStrength == "Good"){
            $(".pass-hint").css({
                "color" : "#f60"
            });
        }
        else if(passStrength == "Strong"){
            $(".pass-hint").css({
                "color" : "#050"
            });
        }
        else if(passStrength == "Very Strong"){
            $(".pass-hint").css({
                "color" : "#0f0"
            });
        }

        if(newPassword != ""){
            $(".pass-container").show();
        }
        else{
            $(".pass-container").hide();
        }
    });

    $('#validatePassword').click(function() {
        var dataLoadingLabel = $(this).attr("data-loading-label");

        var password = $('#password').val();
        var repeatPassword = $('#repeat-password').val();

        if ($.trim(password).length == 0) {
            $(".login-form").find(".error-message").html("Please enter your password.");
            $("#password").parents(".form").addClass("error");
            e.preventDefault();
        }

        if (($.trim(password).length != 0) & ($.trim(password).length < 6)) {
            $(".login-form").find(".error-message").html("Password must be at least 6 characters.");
            $("#password").parents(".form").addClass("error");
            e.preventDefault();
        }

        if (($.trim(password).length != 0) & ($.trim(repeatPassword).length == 0)) {
            $(".login-form").find(".error-message").html("Please repeat your password.");
            $("#password").parents(".form").addClass("error");
            e.preventDefault();
        }


        if (password != repeatPassword) {
            $(".login-form").find(".error-message").html("Password did not match the repeated password.");
            $("#password").parents(".form").addClass("error");
            e.preventDefault();
        }

        else {
            $(".login-form").find(".error-message").html("");
            $("#password").parents(".form").removeClass("error");
            $(this).addClass("disabled").html("<div class='ui active tiny inline loader inverted'></div> "+dataLoadingLabel);
        }
    });

    var mySVG = $('.svg').drawsvg();

    mySVG.drawsvg('animate');
})(jQuery);