(function($) {
    $('#startDate').datepicker({
        "container": "#startDatepicker",
    	"autoclose": true,
    	"format": "D, MM dd, yyyy",
    	'templates': {
    		"leftArrow": '<i class="fa fa-angle-left"></i>',
    		"rightArrow": '<i class="fa fa-angle-right"></i>'
    	},
        "todayHighlight": true,
        "startDate": new Date()
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#endDate').datepicker('setStartDate', minDate);
    });;


    $('#endDate').datepicker({
        "container": "#endDatepicker",
        "autoclose": true,
        "format": "D, MM dd, yyyy",
        "useCurrent": false,
        'templates': {
            "leftArrow": '<i class="fa fa-angle-left"></i>',
            "rightArrow": '<i class="fa fa-angle-right"></i>'
        }
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#startDate').datepicker('setEndDate', minDate);
    });;

    $(".date-dropdown").on("click", function(){
        $(this).parents(".form").find(".field").trigger("focus");
    })
})(jQuery);