(function($) {
    $('#date').datepicker({
        "container": "#datepicker",
    	"autoclose": true,
    	"format": "D, MM dd, yyyy",
    	"todayHighlight": true,
    	'templates': {
    		"leftArrow": '<i class="fa fa-angle-left"></i>',
    		"rightArrow": '<i class="fa fa-angle-right"></i>'
    	}
    });

    $("#message").on("change", function(){
    	$("#remarks").text(this.value);
    });

    $("#date").on("change", function(){
    	$("#disbursementDate").text(this.value);
    });

    $("#triggerUpload").on("click", function(){
    	$("#uploadFile").trigger("click");
    });

    $("#uploadFile").on("change", function(){
    	var uploadFile = $(this).val();

    	if(uploadFile != ""){
    		$(".upload-note").append("<i class=\"icon fa fa-check-circle\"></i>"+
    											"<a id=\"previewFile\" class=\"preview-file\">"+ uploadFile +"</a>");
    		$("#fileName").text(this.value);
            var dataLoadingLabel = $("#triggerUpload").attr("data-loading-label");
            $("#triggerUpload").addClass("disabled").html("<div class='ui active tiny inline loader inverted'></div> "+dataLoadingLabel);
    	};
    });

    $("#submitSummary").on("click", function(){
        if($("#confirmSummary").prop('checked') == false){
            $(this).parents(".modal").find(".error-message").text("Please check the confirmation checkbox");
            $(".ui.modal").modal("refresh");
        }else{
            var dataLoadingLabel = $(this).attr("data-loading-label");
            $(this).parents(".modal").find(".error-message").text("");
            $(".ui.modal").modal("refresh");
            $(this).addClass("disabled").html("<div class='ui active tiny inline loader inverted'></div> "+dataLoadingLabel);
        }
    });

    $("#date-dropdown").on("click", function(){
        $("#date").trigger("focus");
    })
})(jQuery);