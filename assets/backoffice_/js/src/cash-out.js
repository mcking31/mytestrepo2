(function($) {
    $(document).ready(function(e) {
        var pin = "1234";

        $("#submitPin").on("click", function(){
            var pinVal = $("#otp").val();
            var currentAttemptCount = $("#attempts").val();
            
            if(pinVal == "1234"){
                $("#verified").modal({
                    duration: "50",
                    closable: false,
                    allowMultiple: false,
                    transition: "fade left"
                }).modal("show").modal("refresh");
            }
            else{
                var attemptAdd = parseInt(currentAttemptCount) + 1;
                if(attemptAdd == 1){
                    $("#pin .alert-group").slideUp(200, function(){
                        var once = false;
                        $("#pin .alert-group").html(
                            "<div class=\"alert error\">"+
                                "<div class=\"content\">"+
                                    "<p class=\"title\">"+
                                        "Invalid PIN"+
                                    "</p>"+
                                    "<p class=\"description\">"+
                                        "1 of 3 attempts remaining."+
                                    "</p>"+
                                "</div>"+
                            "</div>"
                        );
                    });

                    $("#pin .alert-group").slideDown(200);
                }

                if(attemptAdd == 2){
                    var once = false;
                    $("#pin .alert-group").slideUp(200, function(){
                        $("#pin .alert-group").html(
                            "<div class=\"alert error\">"+
                                "<div class=\"content\">"+
                                    "<p class=\"title\">"+
                                        "Invalid PIN. Last Attempt."+
                                    "</p>"+
                                    "<p class=\"description\">"+
                                        "Entering another invalid PIN will end the transaction."+
                                    "</p>"+
                                "</div>"+
                            "</div>"
                        );

                        $("#pin .alert-group").slideDown(200);
                    });
                }

                if(attemptAdd == 3){
                    var once = false;
                    $("#pin .alert-group").slideUp(200, function(){
                        $("#pin .alert-group").html(
                            "<div class=\"alert error\">"+
                                "<div class=\"content\">"+
                                    "<p class=\"title\">"+
                                        "Transaction Ended"+
                                    "</p>"+
                                    "<p class=\"description\">"+
                                        "You have reached the maximum number of PIN attempts."+
                                    "</p>"+
                                "</div>"+
                            "</div>"
                        );
                    });

                    $("#pin .alert-group").slideDown(200);


                    $("#submitPin").addClass("disabled").attr("disabled", true);
                }

                $("#attempts").val(attemptAdd);
            }
        });

        $("#requestPin").on("click", function(){
            var currentRequestCount = $("#requestAttempts").val();
            var fixedAttempts = 3;
            var requestAttemptAdd = parseInt(currentRequestCount) + 1;
            var remainingAttempts = fixedAttempts - requestAttemptAdd;
            if(requestAttemptAdd <= 2){
                $("#pin .alert-group").slideUp(200, function(){
                    var once = false;
                    $("#pin .alert-group").html(
                        "<div class=\"alert success\">"+
                            "<div class=\"content\">"+
                                "<p class=\"title\">"+
                                    "Message Sent"+
                                "</p>"+
                                "<p class=\"description\">"+
                                    "TrueMoney sent a one-time PIN to the customer via SMS. "+ remainingAttempts +" requests remaining."+
                                "</p>"+
                            "</div>"+
                        "</div>"
                    );

                    $("#pin .alert-group").slideDown(200);
                    $("#attempts").val("0");
                    $("#submitPin").removeClass("disabled").attr("disabled", false);
                });
            }

            if(requestAttemptAdd == 3){
                $("#pin .alert-group").slideUp(200, function(){
                    var once = false;
                    $("#pin .alert-group").html(
                        "<div class=\"alert success\">"+
                            "<div class=\"content\">"+
                                "<p class=\"title\">"+
                                    "Limit Reached"+
                                "</p>"+
                                "<p class=\"description\">"+
                                    "You have reached the maximum number of requests.Type the correct PIN or cancel the transaction."+
                                "</p>"+
                            "</div>"+
                        "</div>"
                    );

                    $("#pin .alert-group").slideDown(200);
                    $("#requestPin").remove();
                    $("#attempts").val("0");
                    $("#submitPin").removeClass("disabled").attr("disabled", false);
                });
            }

            $("#requestAttempts").val(requestAttemptAdd);
        });
    });
})(jQuery);