(function($) {
	$("#settlementSubmit").on("click", function(){
		var walletId = $("#wallet-id").val();
		var amount = $("#amount").val();
		var validWalletId = "123412341234";
		var error = false;

		if(walletId.length === 0){
			error = true;
			$("#wallet-id").parents(".form").addClass("error");
			$("#wallet-id").parents(".form").find(".note").text("Please enter the wallet ID");
			$(".alert-group").html(
                "<div class=\"alert error\">"+
                    "<div class=\"content\">"+
                        "<p class=\"title\">"+
                            "Settlement Failed"+
                        "</p>"+
                        "<p class=\"description\">"+
                            "Please try again."+
                        "</p>"+
                    "</div>"+
                    "<i class=\"icon fa fa-times close\"></i>"+
                "</div>"
            );
            alertClose();
		// }else if(walletId !== validWalletId){
			// error = true;
			// $("#wallet-id").parents(".form").addClass("error");
			// $("#wallet-id").parents(".form").find(".note").text("Wallet ID doesn't exist.");
			// $(".alert-group").html(
                // "<div class=\"alert error\">"+
                    // "<div class=\"content\">"+
                        // "<p class=\"title\">"+
                            // "Settlement Failed"+
                        // "</p>"+
                        // "<p class=\"description\">"+
                            // "Please try again."+
                        // "</p>"+
                    // "</div>"+
                    // "<i class=\"icon fa fa-times close\"></i>"+
                // "</div>"
            // );
            // alertClose();
		}else{
			error = false;
        	$("#wallet-id").parents(".form").removeClass("error");
			$("#wallet-id").parents(".form").find(".note").text("");
        	if(amount.length === 0){
        		error = true;
        		$("#amount").parents(".form").addClass("error");
				$("#amount").parents(".form").find(".note").text("Please enter the amount");
				$(".alert-group").html(
	                "<div class=\"alert error\">"+
	                    "<div class=\"content\">"+
	                        "<p class=\"title\">"+
	                            "Settlement Failed"+
	                        "</p>"+
	                        "<p class=\"description\">"+
	                            "Please try again."+
	                        "</p>"+
	                    "</div>"+
	                    "<i class=\"icon fa fa-times close\"></i>"+
	                "</div>"
	            );
	            alertClose();
        	}else if(!$.isNumeric(amount)){
        		error = true;
        		$("#amount").parents(".form").addClass("error");
				$("#amount").parents(".form").find(".note").text("Invalid character input");
				$(".alert-group").html(
	                "<div class=\"alert error\">"+
	                    "<div class=\"content\">"+
	                        "<p class=\"title\">"+
	                            "Settlement Failed"+
	                        "</p>"+
	                        "<p class=\"description\">"+
	                            "Please try again."+
	                        "</p>"+
	                    "</div>"+
	                    "<i class=\"icon fa fa-times close\"></i>"+
	                "</div>"
	            );
	            alertClose();
        	}else{
        		error = false;
        		$(".alert-group").html("");
        	}
		}

		if(error == false){
			var modal = $("#settlementSubmit").attr("data-modal");
            $(modal).modal({
                duration: "50",
                closable: false
            }).modal("show");

    		$("#amount").parents(".form").removeClass("error");
			$("#amount").parents(".form").find(".note").text("");
		}
	});

	$("#confirm").on("click", function(){
		var $this = $(this);
		
		$.post(
			baseurl + 'prefunding/prefund',
			{
				'wallet_id' : $("select[id='company-name']").val(),
				'amount' : $("input[id='amount']").val()
			}
		).done(function(data){
			$this.parents(".ui.modal").modal("hide");
			var dataLoadingLabel = $this.attr("data-loading-label");
			$this.removeClass("disabled").html("Confirm");
			
			if( data.response ){
				$(".alert-group").html(
					"<div class=\"alert success\">"+
						"<div class=\"content\">"+
							"<p class=\"title\">"+
								"Settlement Completed"+
							"</p>"+
							"<p class=\"description\">"+
								"An amount of <b> P "+ numeral($("#amount").val()).format('0,0.00') + "</b> from <b>"+ $("#wallet-id").val() +"</b> is settled on " + data.transaction_date +
							"</p>"+
						"</div>"+
						"<i class=\"icon fa fa-times close\"></i>"+
					"</div>"
				);
			} else {
				$(".alert-group").html(
	                "<div class=\"alert error\">"+
	                    "<div class=\"content\">"+
	                        "<p class=\"title\">"+
	                            "Settlement Failed"+
	                        "</p>"+
	                        "<p class=\"description\">"+
	                            "Please try again."+
	                        "</p>"+
	                    "</div>"+
	                    "<i class=\"icon fa fa-times close\"></i>"+
	                "</div>"
	            );
			}
			
			$("#wallet-id").val("");
			$("#amount").val("");
			alertClose();
		});
	});

	$("#confirmSettle").on("click", function(){
		if($(this).is(':checked')){
			$("#confirm").removeClass("disabled").removeAttr("disabled");
		}
		else{
			$("#confirm").addClass("disabled");
		}
	})

	function alertClose(){
		$(".alert .close").on("click", function(){
	        $(this).parents(".alert").fadeOut(300, function(){
	            $(this).remove();
	        });
	    });

	    $(".form.error .field").on("change keyup paste", function(){
	        if($(this).val() == ""){
	            $(this).parents(".form").removeClass("error");
	            $(this).parents(".form").find(".note").text("");
	        }
	    });
	}
})(jQuery);