(function($) {
	$("#linkSubmit").on("click", function(){
		var walletId = $("#wallet-id").val();
		var cardNumber = $("#card-number").val();
		var validWalletId = "123412341234";
		var validCardNumber = "432143214321";
		var error = false;

		if(walletId.length === 0){
			error = true;
			$("#wallet-id").parents(".form").addClass("error");
			$("#wallet-id").parents(".form").find(".note").text("Please enter the wallet ID");
			$(".alert-group").html(
                "<div class=\"alert error\">"+
                    "<div class=\"content\">"+
                        "<p class=\"title\">"+
                            "Card Linking Failed"+
                        "</p>"+
                        "<p class=\"description\">"+
                            "Please try again."+
                        "</p>"+
                    "</div>"+
                    "<i class=\"icon fa fa-times close\"></i>"+
                "</div>"
            );
            alertClose();
		}else if(walletId !== validWalletId){
			error = true;
			$("#wallet-id").parents(".form").addClass("error");
			$("#wallet-id").parents(".form").find(".note").text("Wallet ID doesn't exist.");
			$(".alert-group").html(
                "<div class=\"alert error\">"+
                    "<div class=\"content\">"+
                        "<p class=\"title\">"+
                            "Card Linking Failed"+
                        "</p>"+
                        "<p class=\"description\">"+
                            "Please try again."+
                        "</p>"+
                    "</div>"+
                    "<i class=\"icon fa fa-times close\"></i>"+
                "</div>"
            );
            alertClose();
		}else{
			error = false;
        	$("#wallet-id").parents(".form").removeClass("error");
			$("#wallet-id").parents(".form").find(".note").text("");
			if(cardNumber.length === 0){
				error = true;
				$("#card-number").parents(".form").addClass("error");
				$("#card-number").parents(".form").find(".note").text("Please enter the card number");
				$(".alert-group").html(
	                "<div class=\"alert error\">"+
	                    "<div class=\"content\">"+
	                        "<p class=\"title\">"+
	                            "Card Linking Failed"+
	                        "</p>"+
	                        "<p class=\"description\">"+
	                            "Please try again."+
	                        "</p>"+
	                    "</div>"+
	                    "<i class=\"icon fa fa-times close\"></i>"+
	                "</div>"
	            );
	            alertClose();
			}else if(cardNumber !== validCardNumber){
				error = true;
				$("#card-number").parents(".form").addClass("error");
				$("#card-number").parents(".form").find(".note").text("Card number doesn't exist.");
				$(".alert-group").html(
	                "<div class=\"alert error\">"+
	                    "<div class=\"content\">"+
	                        "<p class=\"title\">"+
	                            "Card Linking Failed"+
	                        "</p>"+
	                        "<p class=\"description\">"+
	                            "Please try again."+
	                        "</p>"+
	                    "</div>"+
	                    "<i class=\"icon fa fa-times close\"></i>"+
	                "</div>"
	            );
	            alertClose();
			}else{
				error = false;
	        	$("#card-number").parents(".form").removeClass("error");
				$("#card-number").parents(".form").find(".note").text("");
			}
        	
		}

		if(error == false){
			var modal = $("#linkSubmit").attr("data-modal");
            $(modal).modal({
                duration: "50",
                closable: false
            }).modal("show");
		}
	});

	$("#confirm").on("click", function(){
		$(this).parents(".ui.modal").modal("hide");
		var dataLoadingLabel = $(this).attr("data-loading-label");
        $(this).removeClass("disabled").html("Confirm");
		$(".alert-group").html(
            "<div class=\"alert success\">"+
                "<div class=\"content\">"+
                    "<p class=\"title\">"+
                        "Card Linking Completed"+
                    "</p>"+
                    "<p class=\"description\">"+
                        "<b>#"+ $("#wallet-id").val() + "</b> is linked to <b>#"+ $("#card-number").val() +"</b> on Monday, September 18, 2017 11:00 AM"+
                    "</p>"+
                "</div>"+
                "<i class=\"icon fa fa-times close\"></i>"+
            "</div>"
        );

		$("#wallet-id").val("");
		$("#card-number").val("");
		alertClose();
	});

	$("#confirmLink").on("click", function(){
		if($(this).is(':checked')){
			$("#confirm").removeClass("disabled").removeAttr("disabled");
		}
		else{
			$("#confirm").addClass("disabled");
		}
	})

	function alertClose(){
		$(".alert .close").on("click", function(){
	        $(this).parents(".alert").fadeOut(300, function(){
	            $(this).remove();
	        });
	    });

	    $(".form.error .field").on("change keyup paste", function(){
	        if($(this).val() == ""){
	            $(this).parents(".form").removeClass("error");
	            $(this).parents(".form").find(".note").text("");
	        }
	    });
	}
})(jQuery);