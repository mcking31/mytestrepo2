(function($) {
	$("#adjustSubmit").on("click", function(){
		var walletId = $("#source-wallet-id").val();
		var walletIdTarget = $("#target-wallet-id").val();
		var amount = $("#amount").val();
		var validWalletId = "123412341234";
		var validWalletIdTarget = "432143214321";
		var error = false;

		if(walletId.length === 0){
			error = true;
			$("#source-wallet-id").parents(".form").addClass("error");
			$("#source-wallet-id").parents(".form").find(".note").text("Please enter the source wallet ID");
			$(".alert-group").html(
                "<div class=\"alert error\">"+
                    "<div class=\"content\">"+
                        "<p class=\"title\">"+
                            "Adjustment Failed"+
                        "</p>"+
                        "<p class=\"description\">"+
                            "Please try again."+
                        "</p>"+
                    "</div>"+
                    "<i class=\"icon fa fa-times close\"></i>"+
                "</div>"
            );
            alertClose();
		}else if(walletId !== validWalletId){
			error = true;
			$("#source-wallet-id").parents(".form").addClass("error");
			$("#source-wallet-id").parents(".form").find(".note").text("Source wallet ID doesn't exist.");
			$(".alert-group").html(
                "<div class=\"alert error\">"+
                    "<div class=\"content\">"+
                        "<p class=\"title\">"+
                            "Adjustment Failed"+
                        "</p>"+
                        "<p class=\"description\">"+
                            "Please try again."+
                        "</p>"+
                    "</div>"+
                    "<i class=\"icon fa fa-times close\"></i>"+
                "</div>"
            );
            alertClose();
		}else{
			error = false;
        	$("#source-wallet-id").parents(".form").removeClass("error");
			$("#source-wallet-id").parents(".form").find(".note").text("");
			if(walletIdTarget.length === 0){
				error = true;
				$("#target-wallet-id").parents(".form").addClass("error");
				$("#target-wallet-id").parents(".form").find(".note").text("Please enter the target wallet ID");
				$(".alert-group").html(
	                "<div class=\"alert error\">"+
	                    "<div class=\"content\">"+
	                        "<p class=\"title\">"+
	                            "Adjustment Failed"+
	                        "</p>"+
	                        "<p class=\"description\">"+
	                            "Please try again."+
	                        "</p>"+
	                    "</div>"+
	                    "<i class=\"icon fa fa-times close\"></i>"+
	                "</div>"
	            );
	            alertClose();
			}else if(walletIdTarget !== validWalletIdTarget){
				error = true;
				$("#target-wallet-id").parents(".form").addClass("error");
				$("#target-wallet-id").parents(".form").find(".note").text("Target wallet ID doesn't exist.");
				$(".alert-group").html(
	                "<div class=\"alert error\">"+
	                    "<div class=\"content\">"+
	                        "<p class=\"title\">"+
	                            "Adjustment Failed"+
	                        "</p>"+
	                        "<p class=\"description\">"+
	                            "Please try again."+
	                        "</p>"+
	                    "</div>"+
	                    "<i class=\"icon fa fa-times close\"></i>"+
	                "</div>"
	            );
	            alertClose();
			}else{
				error = false;
	        	$("#target-wallet-id").parents(".form").removeClass("error");
				$("#target-wallet-id").parents(".form").find(".note").text("");

				if(amount.length === 0){
	        		error = true;
	        		$("#amount").parents(".form").addClass("error");
					$("#amount").parents(".form").find(".note").text("Please enter the amount");
					$(".alert-group").html(
		                "<div class=\"alert error\">"+
		                    "<div class=\"content\">"+
		                        "<p class=\"title\">"+
		                            "Adjustment Failed"+
		                        "</p>"+
		                        "<p class=\"description\">"+
		                            "Please try again."+
		                        "</p>"+
		                    "</div>"+
		                    "<i class=\"icon fa fa-times close\"></i>"+
		                "</div>"
		            );
		            alertClose();
	        	}else if(!$.isNumeric(amount)){
	        		error = true;
	        		$("#amount").parents(".form").addClass("error");
					$("#amount").parents(".form").find(".note").text("Invalid character input");
					$(".alert-group").html(
		                "<div class=\"alert error\">"+
		                    "<div class=\"content\">"+
		                        "<p class=\"title\">"+
		                            "Adjustment Failed"+
		                        "</p>"+
		                        "<p class=\"description\">"+
		                            "Please try again."+
		                        "</p>"+
		                    "</div>"+
		                    "<i class=\"icon fa fa-times close\"></i>"+
		                "</div>"
		            );
		            alertClose();
	        	}else{
	        		error = false;
	        		$(".alert-group").html("");
	        	}
			}
        	
		}

		if(error == false){
			var modal = $("#adjustSubmit").attr("data-modal");
            $(modal).modal({
                duration: "50",
                closable: false
            }).modal("show");

    		$("#amount").parents(".form").removeClass("error");
			$("#amount").parents(".form").find(".note").text("");
		}
	});

	$("#confirm").on("click", function(){
		$(this).parents(".ui.modal").modal("hide");
		var dataLoadingLabel = $(this).attr("data-loading-label");
        $(this).removeClass("disabled").html("Confirm");
		$(".alert-group").html(
            "<div class=\"alert success\">"+
                "<div class=\"content\">"+
                    "<p class=\"title\">"+
                        "Adjustment Completed"+
                    "</p>"+
                    "<p class=\"description\">"+
                        "An amount of <b> P "+ numeral($("#amount").val()).format('0,0.00') + "</b> is debited from <b>"+ $("#source-wallet-id").val() + "</b> and credited to <b>"+ $("#target-wallet-id").val() +"</b> on Monday, September 18, 2017 11:00 AM"+
                    "</p>"+
                "</div>"+
                "<i class=\"icon fa fa-times close\"></i>"+
            "</div>"
        );

		$("#source-wallet-id").val("");
		$("#target-wallet-id").val("");
		$("#amount").val("");
		alertClose();
	});

	$("#confirmAdjust").on("click", function(){
		if($(this).is(':checked')){
			$("#confirm").removeClass("disabled").removeAttr("disabled");
		}
		else{
			$("#confirm").addClass("disabled");
		}
	});
	

	function alertClose(){
		$(".alert .close").on("click", function(){
	        $(this).parents(".alert").fadeOut(300, function(){
	            $(this).remove();
	        });
	    });

	    $(".form.error .field").on("change keyup paste", function(){
	        if($(this).val() == ""){
	            $(this).parents(".form").removeClass("error");
	            $(this).parents(".form").find(".note").text("");
	        }
	    });
	}
})(jQuery);