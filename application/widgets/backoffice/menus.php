<?php

class Menus extends Widget {

    public function display($data) {
		$this->load->library('query');
		
		$group_id = $this->common->get_session('group_id');
		
		$data['menus'] = $this->is_user_menu(
			$this->fetch_menus(),
			$group_id
		);
		
		$this->view('widgets/backoffice/menus', $data);
    }
	
	public function fetch_menus( $conditions = array('parent_id' => NULL), $arr = array() ){
		
		if( $this->count_menus( $conditions ) > 0 ){
			$_menus = $this->query->select(
				array(
					'table' => 'cms_menus',
					'conditions' => $conditions,
					'order' => 'order_number'
				),
				false
			);
			
			foreach( $_menus as $key => $values ){
				if( isset( $conditions['is_active'] ) )
					$conditions = array( 'parent_id' => $values['menu_id'], 'is_active' => $conditions['is_active'] );
				else
					$conditions = array( 'parent_id' => $values['menu_id'] );
				
				array_push( $arr, $values );
				
				if( $this->count_menus( $conditions ) > 0 ){
					$arr[$key]['children'] = $this->fetch_menus( $conditions, array() );
				}
			}
		}
		
		return $arr;
	}
	
	public function count_menus( $conditions = array() ){
		return $this->query->select(
			array(
				'table' => 'cms_menus',
				'conditions' => $conditions
			),
			false,
			true
		);
	}
	
	public function is_user_menu( $arr = array(), $group_id ){		
		foreach( $arr as $key => $values ){
			if( 
				$this->query->select(
					array(
						'table' => 'user_group_menus',
						'conditions' => array(
							'group_id' => $group_id,
							'menu_id' => $values['menu_id']
						)
					),
					false,
					true
				) > 0
			)
				$arr[$key]['has_access'] = true;
			else
				$arr[$key]['has_access'] = false;
			
			if( isset( $values['children'] ) )
				$arr[$key]['children'] = $this->is_user_menu( $values['children'], $group_id );
		}
		
		return $arr;
	}
    
}