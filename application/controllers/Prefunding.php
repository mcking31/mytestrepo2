<?php
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Prefunding extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('api');
		$this->load->helper('form');
	}
	
	public function index(){
		$data = [];
		$user_data = $this->common->get_session('data');
		
		$ucore = $this->api->getPartners( 
			[
				
			] 
		);
		
		if( $ucore['Result'] == '0' ):
			$data['companies'] = array_column($ucore['data'], 'Name', 'WalletId');
		else:
			$data['companies'] = [];
		endif;
		
		$this->template->build_template(
			'Prefunding',
			array(
				array(
					'view' => 'prefunding',
					'data' => $data
				)
			),
			array(
				'assets/js/autoNumeric-master/autoNumeric.js',
				'assets/module_js/prefunding.js'
			),
			array(),
			array(),
			'backoffice'
		);
	}
	
	public function prefund(){
		
		$data['response'] = FALSE;
		
		try{
			$data['message'] = "";
			$data['response'] = FALSE;
			
			// CORE
			$this->load->library('api');
			$result = $this->api->insPrefundingAmount(
					[
						'transactionAmount' => $this->input->post('amount'),
						'walletId' => $this->input->post('wallet_id')
					]
				);
			
			if( $result['Result'] == '0' ):
				$data = [
					'response' => TRUE,
					'message' => $result['Message'],
					'transaction_date' => date('l, F d, Y h:s a'),
					'trn' => $result['ReferenceID']
				];
			else:
				$data = [
					'response' => FALSE,
					'message' => $result['Message'],
					'trn' => $result['ReferenceID']
				];
			endif;
			// END CORE
			
		} catch( Exception $e ) {
			$data['message'] = $e->getMessage();
		}
		
		header( 'Content-Type: application/x-json' );
		echo json_encode( $data );
	}
}