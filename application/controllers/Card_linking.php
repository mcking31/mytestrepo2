<?php
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Card_linking extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$data = [];
		
		$this->template->build_template(
			'Card Linking',
			array(
				array(
					'view' => 'card-linking',
					'data' => $data
				)
			),
			array(
				'assets/js/autoNumeric-master/autoNumeric.js',
				'assets/module_js/card-linking.js'
			),
			array(),
			array(),
			'backoffice'
		);
	}
	
	public function action(){
		
		$data['response'] = FALSE;
		
		try{
			$data['message'] = "";
			$data['response'] = FALSE;
			
			// CORE
			$this->load->library('api');
			$result = $this->api->linkCardToWallet(
					[
						'walletId' => $this->input->post('wallet_id'),
						'cardNumber' => $this->input->post('card_number'),
						'adminUserId' => $this->common->get_session('adminUsersId')
					]
				);
			
			if( $result['Result'] == 0 ):
				$data = [
					'response' => TRUE,
					'message' => $result['Message'],
					'transaction_date' => date('l, F d, Y h:s a'),
					'trn' => $result["ReferenceID"]
				];
			else:
				$data = [
					'response' => FALSE,
					'message' => $result['Message'],
					'trn' => $result["ReferenceID"]
				];
			endif;
			// END CORE
			
		} catch( Exception $e ) {
			$data['message'] = $e->getMessage();
		}
		
		header( 'Content-Type: application/x-json' );
		echo json_encode( $data );
	}
}