<?php
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Customer_profile extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$data = [];
		
		$this->template->build_template(
			'Customer Profile',
			array(
				array(
					'view' => 'customer-profile',
					'data' => $data
				)
			),
			array(
				'assets/module_js/customer-profile.js'
			),
			array(),
			array(),
			'backoffice'
		);
	}
	
	public function search(){
		
		$data['response'] = FALSE;
		
		try{
			
			// CORE
			$this->load->library('api');
			$result = $this->api->getProfileWallet(
					[
						'walletId' => $this->input->post('walletid'),
						'LastName' => $this->input->post('lastname'),
						'FirstName' => $this->input->post('firstname')
					]
				);
				
			// debug($result);
			
			if( $result['Result'] == '0' ):
				$data = [
					'response' => isset( $result['data'][0] ) ? TRUE : FALSE,
					'message' => $result['Message'],
					'data' => isset( $result['data'][0] ) ? $result['data'][0] : null,
					'trn' => $result['ReferenceID']
				];
			else:
				$data = [
					'response' => FALSE,
					'message' => $result['Message'],
					'trn' => $result['ReferenceID']
				];
			endif;
			// END CORE
			
		} catch( Exception $e ) {
			$data['message'] = $e->getMessage();
		}
		
		header( 'Content-Type: application/x-json' );
		echo json_encode( $data );
	}
}