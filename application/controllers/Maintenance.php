<?php
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Maintenance extends CI_Controller {
	var $tbls = array();
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library( 'query');
		
		$this->tbls = array(
			md5('cms_menus') => array(
				'table' => 'cms_menus',
				'primary_key' => 'cms_menus.menu_id',
				'primary_key2' => 'menu_id',
				'fields' => array( 'icon', 'title', 'url', 'parent_id', 'order_number', 'is_active' ),
				'field_list' => 'cms_menus.icon,cms_menus.title,cms_menus.url,cms_menus.parent_id,cms_menus.order_number,cms_menus.is_active,parent_menu.title as parent_title',
				'joins' => array(
					'cms_menus as parent_menu' => array(
						'parent_menu.menu_id' => 'cms_menus.menu_id'
					),
				),
				'validations' => array(
					array(
						'field' => 'title',
						'label' => 'Menu title',
						'rules' => 'required'
					),
					array(
						'field' => 'order_number',
						'label' => 'Order number',
						'rules' => 'required|numeric'
					)
				),
				'extra_validations' => array(
					'create' => array(
						array(
							'field' => 'title',
							'label' => 'Menu title',
							'rules' => 'required'
						)
					)
				)
			),
			md5('user_groups') => array(
				'table' => 'user_groups',
				'primary_key' => 'group_id',
				'primary_key2' => 'group_id',
				'fields' => array( 'group_id', 'user_group' ),
				'field_list' => 'user_groups.group_id, user_groups.user_group',
				'validations' => array(
					array(
						'field' => 'user_group',
						'label' => 'User Group',
						'rules' => 'required'
					)
				),
				'extra_validations' => array(
					'create' => array(
						'field' => 'user_group',
						'label' => 'User Group',
						'rules' => 'required'
					)
				)
			),
			md5('user_group_menus') => array(
				'table' => 'user_group_menus',
				'primary_key' => 'id',
				'primary_key2' => 'id',
				'fields' => array( 'menu_id', 'group_id' ),
				'field_list' => 'user_groups.user_group, cms_menus.title',
				'joins' => array(
					'cms_menus' => array(
						'cms_menus.menu_id' => 'user_group_menus.menu_id'
					),
					'user_groups' => array(
						'user_groups.group_id' => 'user_group_menus.group_id'
					)
				),
				'validations' => array(
					array(
						'field' => 'group_id',
						'label' => 'User Group',
						'rules' => 'required'
					),
					array(
						'field' => 'menu_id',
						'label' => 'Menu',
						'rules' => 'required'
					)
				)
			)
		);
	}
	
	public function test(){
		echo md5('user_group_menus');
	}
	
	public function index(){
		$data = [
			'groups' => array_column(
				$this->query->select(
					[
						'table' => 'user_groups'
					]
				),
				'user_group',
				'group_id'
			),
			'menus' => array_column(
				$this->query->select(
					[
						'table' => 'cms_menus',
						'conditions' => array(
							'is_active' => 1
						)
					]
				),
				'title',
				'menu_id'
			)
		];
		$this->load->library('template');
		
		
		$this->template->build_template(
			'Manage lookup tables',
			array(
				array(
					'view' => 'index',
					'data' => $data
				),
				array(
					'view' => 'default_views/modals',
					'data' => array()
				)
			),
			array(
				'assets/default.js',
				'assets/maintenance.js',
				'assets/datatable/data-table.js'
			),
			array(
				'assets/datatable/data-table.css'
			),
			array(),
			'default'
		);
	}

	public function action(){
		$data['response'] = false;
		
		try{			
			$tbl = $this->input->post('tbl');
			
			if( isset( $this->tbls[ $tbl ] ) ){
				$primary_id = $this->input->post( $this->tbls[ $tbl ]['primary_key'] );
				
				$primary_id = ( strlen( $primary_id ) == 0 ) ? $this->input->post( $this->tbls[ $tbl ]['primary_key2'] ) : $primary_id;
				
				$this->load->library('form_validation');
				
				if( strlen( trim( $primary_id ) ) > 0 ){
					// update
					$this->form_validation->set_rules( $this->tbls[ $tbl ]['validations'] );
					if( isset( $this->tbls[ $tbl ]['extra_validations']['update'] ) )
						$this->form_validation->set_rules( $this->tbls[ $tbl ]['extra_validations']['update'] );
					
					if ($this->form_validation->run() == FALSE){
						$data['errors'] = $this->form_validation->error_array();
					} else {											
						$details = array();
						
						foreach( $this->tbls[ $tbl ]['fields'] as $key => $values ){
							if( !empty( $this->input->post( $values ) ) )
								$details[ $values ] = $this->input->post( $values );
						}
						
						$_res = $this->query->update(
							$this->tbls[ $tbl ]['table'],
							array(
								$this->tbls[ $tbl ]['primary_key'] => $primary_id
							),
							$details
						);
						
						$result = isset( $_res['err_msg'] ) ? FALSE : ( ( $_res ) ? TRUE : FALSE );
						if( $result ){
							$data['response'] = $result;
							$data['message'] = 'Successfully modified.';
						} else
							throw new Exception( isset( $_res['err_msg'] ) ? $_res['err_msg'] : 'Ooops, an error was encountered. Sorry for the inconvenience.' );
					}
				} else {
					// create					
					$this->form_validation->set_rules( $this->tbls[ $tbl ]['validations'] );
					if( isset( $this->tbls[ $tbl ]['extra_validations']['create'] ) )
						$this->form_validation->set_rules( $this->tbls[ $tbl ]['extra_validations']['create'] );
					
					if ($this->form_validation->run() == FALSE){
						$data['errors'] = $this->form_validation->error_array();
					} else {											
						$details = array();
						
						if( !is_array( $this->tbls[ $tbl ]['fields'] ) )
							$this->tbls[ $tbl ]['fields'] = explode(',', $this->tbls[ $tbl ]['fields']);
							
						foreach( $this->tbls[ $tbl ]['fields'] as $key => $values ){
							if( !empty( $this->input->post( $values ) ) )
								$details[ $values ] = $this->input->post( $values );
						}
						
						$_res = $this->query->insert(
							$this->tbls[ $tbl ]['table'],
							$details
						);
						
						$result = isset( $_res['err_msg'] ) ? FALSE : ( ( $_res ) ? TRUE : FALSE );
						if( $result ){
							$data['response'] = $result;
							$data['message'] = 'Successfully added';
						} else
							throw new Exception( isset( $_res['err_msg'] ) ? $_res['err_msg'] : 'Ooops, an error was encountered. Sorry for the inconvenience.' );
					}
				}
			
			} else
				throw new exception( 'Invalid transaction. Sorry for the inconvenience. Please contact your administrator.' );
		} catch( Exception $e ) {
			$data['message'] = $e->getMessage();
		}
		
		header( 'Content-Type: application/x-json' );
		echo json_encode( $data );
	}
	
	public function delete(){
		$data['response'] = false;
		
		try{			
			$tbl = $this->input->post('tbl');
		
			if( isset( $this->tbls[ $tbl ] ) ){
				$primary_id = $this->input->post( 'pkey' );
				
				$data['response'] = $this->query->delete( 
					$this->tbls[ $tbl ]['table'],
					array(
						$this->tbls[ $tbl ]['primary_key'] => $primary_id
					)
				);
				
				if( 
					is_array( $data['response'] ) 
					&&
					isset( $data['response']['err_msg'] )
				)
					$data['message'] = $data['response']['err_msg'];
				else
					$data['message'] = 'Successfully deleted.';
			} else
				throw new exception( 'Invalid transaction. Sorry for the inconvenience. Please contact your administrator.' );
			
		} catch( Exception $e ) {
			$data['message'] = $e->getMessage();
		}
		
		header( 'Content-Type: application/x-json' );
		echo json_encode( $data );
	}
	
	public function get_data_by_id(){
		$data['response'] = false;
		
		try{			
			$tbl = $this->input->post('tbl');
		
			if( isset( $this->tbls[ $tbl ] ) ){
				$primary_id = $this->input->post( 'pkey' );
				
				$data['details'] = $this->query->select( 
					array(
						'table' => $this->tbls[ $tbl ]['table'],
						'conditions' => array(
							$this->tbls[ $tbl ]['primary_key'] => $primary_id
						),
						'fields' => $this->tbls[ $tbl ]['table'] . '.*,' . $this->tbls[ $tbl ]['primary_key']
					),
					FALSE,
					FALSE,
					TRUE
				);
				
				$data['response'] = TRUE;
			} else
				throw new exception( 'Invalid transaction. Sorry for the inconvenience. Please contact your administrator.' );
			
		} catch( Exception $e ) {
			$data['message'] = $e->getMessage();
		}
		
		header( 'Content-Type: application/x-json' );
		echo json_encode( $data );
	}

	public function get_data(){
		$tbl = $this->input->post('tbl');
		$draw = $this->input->post('draw');
		
		if( isset( $this->tbls[ $tbl ] ) ){
			
			$offset = $this->input->post('start');
			$limit = $this->input->post('length');
			$sort = $_POST['order'][0]['dir'];
			$field = $_POST['order'][0]['column'];
			$search_value = $_POST['search']['value'];
			
			$result = array();
			for( $i= $field; $i <= $field; $i++){
				$get_field =  $_POST['columns'][$i]['data'];
				array_push($result,$get_field);
			}
			
			$field_name=implode(",",$result);
			
			$conditions = array();
			// $search_value = 'bwahaha';
			if( strlen( $search_value ) > 0 ){
				$conditions = array_fill_keys( array_column( $_POST['columns'], 'data' ), $search_value );
				if( isset( $conditions[''] ) )	unset( $conditions[''] );
				
				$conditions = array(
					'or_like' => $conditions
				);
			}
			
			$data = $this->query->select(
				array(
					'table' => $this->tbls[ $tbl ]['table'],
					'conditions' => $conditions,
					'joins' => isset( $this->tbls[ $tbl ]['joins'] ) ? $this->tbls[ $tbl ]['joins'] : array(),
					'order' => $field_name . ' ' . $sort,
					'limit' => $limit,
					'start' => $offset,
					'fields' => isset( $this->tbls[ $tbl ]['field_list'] ) ? $this->tbls[ $tbl ]['field_list'] . ', ' . $this->tbls[ $tbl ]['primary_key'] . ' as pkey' : $this->tbls[ $tbl ]['table'] . '.*, ' . $this->tbls[ $tbl ]['primary_key'] . ' as pkey'
				),
				FALSE,
				FALSE,
				FALSE
			);
			
			// debug( $data );
			
			$count = $this->query->select(
				array(
					'table' => $this->tbls[ $tbl ]['table'],
					'joins' => isset( $this->tbls[ $tbl ]['joins'] ) ? $this->tbls[ $tbl ]['joins'] : array(),
					'conditions' => $conditions
				),
				FALSE,
				TRUE
			);

			$results = array(
				"draw" => $draw,
				"recordsTotal" => $count,
				"recordsFiltered" => $count,
				"data"=>$data
			);
			
			echo json_encode($results);
		} else {
			echo json_encode(
				array(
					"draw" => $draw,
					"recordsTotal" => 0,
					"recordsFiltered" => 0,
					"data"=> array()
				)
			);
		}
	}
}