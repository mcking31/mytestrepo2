<div class="item user right-float" id="dropMenu">
	<span class="name">
		<?php echo $firstname . ' ' . $lastname; ?>
		<br>
		<span class="company">Backoffice</span>
	</span>
	<span class="img-holder round">
		<img src="../../assets/images/user-default.png" alt="Company Name" class="img-auto-place">
	</span>
	<span class="dropdown fa fa-angle-down"></span>
	<div class="menu">
		<ul class="list-unstyled">
			<li>
				<a href="<?php echo base_url('logout') ?>">
					<i class="icon fa icon-logout"></i> Logout
				</a>
			</li>
		</ul>
	</div>
</div>