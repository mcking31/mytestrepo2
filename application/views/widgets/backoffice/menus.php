<?php
	$this->load->helper('url');
	
	function format_menu_html( $menus = array(), $html = '', $indention = '---' ){
		foreach( $menus as $key => $values ){
			if( $values['has_access'] ){				
			
				$_a = str_replace(base_url(), '', str_replace(base_url() . '/', '', current_url()));
				
				$active = ( $_a == $values['url'] ) ? "class='active'" : "";
				
				// if( 
					// isset( $values['children'] )
					// &&
					// is_array( $values['children'] ) 
				// ){
					$html .= '<li>
						<a href="' . base_url( $values['url'] ) . '" . ' . $active . '>
							<span class="' . $values['icon'] . '"></span>
							<span class="name">' . $values['title'] . '</span>
						</a>
					</li>';
				// } else {
					// $html .= '<li>
						// <a href="' . base_url( $values['url'] ) . '" . ' . $active . '>
							// <span class="' . $values['icon'] . '"></span>
							// <span class="name">' . $values['title'] . '</span>
						// </a>
					// </li>';
				// }
			}
		}
		
		return $html;
	}
	
	$menu_html = format_menu_html( $menus, '', '' );
?>

<section class="sidebar">
	<ul class="list-unstyled menu">
		<?php echo $menu_html; ?>
	</ul>
</section>