<?php 
	// debug( $statuses );
?>

<div class="row">
<form class="bs-component form-horizontal col-sm-12">
	<ul class="nav nav-tabs">
	  <li class="active"><a href="#menus" data-toggle="tab">Menus</a></li>
	  <li><a href="#user-groups" data-toggle="tab">User Groups</a></li>
	  <li><a href="#user-groups-menus" data-toggle="tab">User Groups Menus</a></li>
	</ul>
	<div id="myTabContent" class="tab-content">
	  <div class="tab-pane fade active in" id="menus">
		<!-- MENUS -->
			<br/>
			<fieldset>
				<legend>
					<h5>
						Portal menus.
					</h5>
				</legend>
			</fieldset>
			<fieldset class="list">
				<legend><h4>List</h4></legend>
				<div class="list-group_statuses">
					<table id="" alt="5df4a012d62701e5ecb16bf4d702f712" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th alt="icon"></th>
								<th alt="title">Menu Title</th>
								<th alt="url">Url</th>
								<th alt="parent_title">Parent Menu</th>
								<th alt="order_number">Order</th>
								<th alt="is_active">Active</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</fieldset>
			<br/>
			<fieldset>
				<legend id="action"><h4>Create</h4></legend>
				<div class="form-group">
					<label for="title" class="col-lg-2 control-label">Menu Title</label>
					<div class="col-lg-10">
						<?php 
							echo form_input( 
								array(
									'name' => 'title',
									'id' => 'title',
									'class' => 'form-control input-sm'
								)
							);
							echo form_hidden( 
								array(
									'menu_id' => ''
								)
							);
							echo form_hidden( 
								array(
									'tbl' => '5df4a012d62701e5ecb16bf4d702f712'
								)
							);
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="url" class="col-lg-2 control-label">Url</label>
					<div class="col-lg-10">
						<?php 
							echo form_input( 
								array(
									'name' => 'url',
									'id' => 'url',
									'class' => 'form-control input-sm'
								)
							);
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="parent_id" class="col-lg-2 control-label">Parent Menu</label>
					<div class="col-lg-10">
						<?php 
							echo form_dropdown( 
								'parent_id',
								[],
								'',
								"name='parent_id' id='parent_id' class='form-control input-sm'"
							);
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="order_number" class="col-lg-2 control-label">Order</label>
					<div class="col-lg-10">
						<?php 
							echo form_input( 
								array(
									'name' => 'order_number',
									'id' => 'order_number',
									'class' => 'form-control input-sm'
								)
							);
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="icon" class="col-lg-2 control-label">Icon</label>
					<div class="col-lg-10">
						<?php 
							echo form_input( 
								array(
									'name' => 'icon',
									'id' => 'icon',
									'class' => 'form-control input-sm'
								)
							);
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="is_active" class="col-lg-2 control-label">Active</label>
					<div class="col-lg-10">
						<?php 
							echo form_checkbox( 
								array(
									'name' => 'is_active',
									'value' => '1',
									'id' => 'is_active',
									'checked' => 'checked'
								)
							);
						?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-2">&nbsp;</div>
					<div class="col-lg-10">
						<a href='javascript:void(0);' class='btn btn-primary btn-xs create_btn' alt='menus'>Create</a>
						<a href='javascript:void(0);' class='btn btn-default btn-xs reset_form' alt='menus'>Reset</a>
					</div>
				</div>
			</fieldset>
		<!-- END OF STATUSES -->		
	  </div>
	  <div class="tab-pane fade in" id="user-groups">
		<br/>
		<fieldset>
			<legend>
				<h5>
					User groupd lookup management.
				</h5>
			</legend>
		</fieldset>
		<fieldset class="list">
			<legend><h4>List</h4></legend>
			<div class="list-group_statuses">
				<table id="" alt="a1ca70124e53a4f2847d6d1c34844be4" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th alt="user_group">User Group</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</fieldset>
		<br/>
		<fieldset>
			<legend id="action"><h4>Create</h4></legend>
			<div class="form-group">
				<label for="user_group" class="col-lg-2 control-label">User Group</label>
				<div class="col-lg-10">
					<?php 
						echo form_input( 
							array(
								'name' => 'user_group',
								'id' => 'user_group',
								'class' => 'form-control input-sm'
							)
						);
						echo form_hidden( 
							array(
								'group_id' => ''
							)
						);
						echo form_hidden( 
							array(
								'tbl' => 'a1ca70124e53a4f2847d6d1c34844be4'
							)
						);
					?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-2">&nbsp;</div>
				<div class="col-lg-10">
					<a href='javascript:void(0);' class='btn btn-primary btn-xs create_btn' alt='user-groups'>Create</a>
					<a href='javascript:void(0);' class='btn btn-default btn-xs reset_form' alt='user-groups'>Reset</a>
				</div>
			</div>
		</fieldset>
	  </div>
	  <div class="tab-pane fade in" id="user-groups-menus">
		<br/>
		<fieldset>
			<legend>
				<h5>
					User groups menus lookup management.
				</h5>
			</legend>
		</fieldset>
		<fieldset class="list">
			<legend><h4>List</h4></legend>
			<div class="list-group_statuses">
				<table id="" alt="597b8d972342870bd38c2f17097e8fc5" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th alt="user_group">User Group</th>
							<th alt="title">Menu Title</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</fieldset>
		<br/>
		<fieldset>
			<legend id="action"><h4>Create</h4></legend>
			<div class="form-group">
				<label for="group_id" class="col-lg-2 control-label">Group</label>
				<div class="col-lg-10">
					<?php 
						echo form_dropdown( 
							'group_id',
							$groups,
							'',
							"name='group_id' id='group_id' class='form-control input-sm'"
						);
						echo form_hidden( 
							array(
								'id' => ''
							)
						);
						echo form_hidden( 
							array(
								'tbl' => '597b8d972342870bd38c2f17097e8fc5'
							)
						);
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="menu_id" class="col-lg-2 control-label">Menu</label>
				<div class="col-lg-10">
					<?php 
						echo form_dropdown( 
							'menu_id',
							$menus,
							'',
							"name='menu_id' id='menu_id' class='form-control input-sm'"
						);
					?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-2">&nbsp;</div>
				<div class="col-lg-10">
					<a href='javascript:void(0);' class='btn btn-primary btn-xs create_btn' alt='user-groups-menus'>Create</a>
					<a href='javascript:void(0);' class='btn btn-default btn-xs reset_form' alt='user-groups-menus'>Reset</a>
				</div>
			</div>
		</fieldset>
	  </div>
	</div>
</form>
</div>