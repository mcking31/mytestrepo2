<div class="alert-group">
	<div class="alert success hidden">
		<div class="content">
			<p class="title">
				Adjustment Successful
			</p>
			<p class="description">
				An amount of <b>P 200,000.00</b> is debited from <b>1234-4234-5435</b> and credited to <b>1234-1446-0876</b> on 08-07-2016 11:00 AM
			</p>
		</div>
		<i class="icon fa fa-times close"></i>
	</div>
	<div class="alert error hidden">
		<div class="content">
			<p class="title">
				Adjustment Failed
			</p>
			<p class="description">
				Please try again.
			</p>
		</div>
		<i class="icon fa fa-times close"></i>
	</div>
</div>
<div class="breadcrumb">
	<div class="breadcrumb-group">
		<div class="active section">Adjustment</div>
	</div>
</div>
<div class="box">
	<div class="box-title">
		<h3>
			Adjustment
		</h3>
	</div>
	<div class="box-content mrg-top-30">
		<div class="numbered-group">
			<div class="numbered-form">
				<div class="description">
					<div class="form label-set">
						<label for="source-wallet-id">Source Wallet ID</label>
						<input type="text" class="field" id="source-wallet-id" placeholder="Enter the source wallet ID" data-mirror="#mirrorSourceId">
						<span class="note"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="numbered-group">
			<div class="numbered-form">
				<div class="description">
					<div class="form label-set">
						<label for="target-wallet-id">Target Wallet ID</label>
						<input type="text" class="field" id="target-wallet-id" placeholder="Enter the target wallet ID" data-mirror="#mirrorTargetId">
						<span class="note"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="numbered-group">
			<div class="numbered-form">
				<div class="description">
					<div class="form label-set amount-form">
						<label for="amount">Enter Amount</label>
						<span class="currency-icon">&#8369;</span>
						<input type="text" class="field mirror-input-amount" data-mirror="#mirrorAmount" id="amount" placeholder="0.00">
						<span class="note"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="numbered-group">
			<div class="numbered-form">
				<div class="number">
					&nbsp;
				</div>
				<div class="description right-align">
					<div class="form">
						<button class="button orange fat" data-modal="#approve" id="adjustSubmit">
							Submit
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="ui modal tiny" id="approve">
	<div class="content">
		<h3 class="center-align">
			Confirm Adjustment
		</h3>
		<div class="mrg-top-20">
			<div class="mrg-top-10">
				<div class="box gray">
					<table class="table basic">
						<tbody>
							<tr>
								<td width="40%" class="bold">Source Wallet ID:</td>
								<td class="nowrap"><span id="mirrorSourceId"></span></td>
							</tr>
							<tr>
								<td width="40%" class="bold">Target Wallet ID:</td>
								<td class="nowrap"><span id="mirrorTargetId"></span></td>
							</tr>
							<tr class="orange">
								<td width="40%" class="bold nowrap">Adjustment Amount: </td>
								<td class="nowrap orange-color bold">P <span id="mirrorAmount"></span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="confirm check">
					<div class="confirm-checkbox">
						<div class="ui checkbox">
						  <input type="checkbox" name="confirm" id="confirmAdjust">
						  <label>&nbsp;</label>
						</div>
					</div>
					<div class="confirm-desc">
						I confirm that the information provided is accurate. <br>
						Clicking <b>CONFIRM</b> will adjust the amount from the source wallet.
					</div>
				</div>
				<div class="response center-align">
					<button class="button gray close">
						Cancel
					</button>
					<button class="button orange loading-after" data-loading-label="Processing" id="confirm" disabled>
						Confirm
					</button>
				</div>
			</div>
		</div>
	</div>
</div>