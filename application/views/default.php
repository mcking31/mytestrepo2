<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="TRUEMONEY">
    <meta name="author" content="TRUEMONEY">
	
	<?php echo $this->template->meta; ?>

    <title>TRUEMONEY <?php echo ( strlen( $this->template->title ) > 0 ) ? " | " . $this->template->title : ""; ?></title>
	
	<!-- Bootstrap Core CSS -->
	<!--<link href="<?php echo base_url('assets/startbootstrap-simple-sidebar-1.0.0/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo base_url('assets/css/yeti/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />

    <!-- Custom CSS -->
	<link href="<?php echo base_url('assets/startbootstrap-simple-sidebar-1.0.0/css/simple-sidebar.css') ?>" rel="stylesheet" type="text/css" />
	
	<!-- JQuery Custom -->
	<link href="<?php echo base_url('assets/js/jquery-ui-1.11.2.custom/jquery-ui.css') ?>" rel="stylesheet" type="text/css" />
	
	<!-- Font Awesome -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	
	<!-- Custom CSS -->
	<link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<script type="text/javascript">
		var baseurl     = '<?php echo site_url(); ?>/';
	</script>
	
	<!-- JQuery -->
	<script src="<?php echo base_url('assets/js/jquery-1.10.2.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
	<script src="<?php echo base_url('assets/startbootstrap-simple-sidebar-1.0.0/js/bootstrap.min.js'); ?>"></script>
	
	<!-- JQuery Custom -->
	<script src="<?php echo base_url('assets/js/jquery-ui-1.11.2.custom/external/jquery/jquery.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-ui-1.11.2.custom/jquery-ui.js'); ?>"></script>
	
	<?php echo $this->template->javascript; ?>
	
	<?php echo $this->template->stylesheet; ?>

</head>

<body>
	<div id="wrapper">
        <!-- Sidebar -->
		<?php 
			echo $this->template->widget("sidebar"); 
		?>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
				<div class="row">
                    <div class="col-lg-12">
						<!-- WARNING --> 
						<?php 
							$warn = $this->session->flashdata('warning');
							if( !empty( $warn ) ){
						?>
							<div class="alert alert-dismissable alert-warning">
							  <button type="button" class="close" data-dismiss="alert">×</button>
							  <h4><?php echo $warn; ?></h4>
							</div>
						<?php
							}
						?>
					</div>
				</div>
                <div class="row">
					<!--<div class="col-lg-2">-->
						<table>
							<tr>
								<td>
									<a href="javascript:void(0);" class="btn btn-default pull-left btn-sm" id="menu-toggle"><i class="fa fa-bars"></i></a>
								</td>
								<td>
									&nbsp;<h1 style="margin-top:0;padding-bottom:10px;margin-left:10px;"><?php echo ( strlen( $this->template->title ) > 0 ) ? $this->template->title : ""; ?></h1>
								</td>
							</tr>
						</table>
					<!--</div>-->
				</div>
                <div class="row">
                    <div class="col-lg-12">
						<?php
							echo $this->template->content; 
						?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Menu Toggle Script -->
    <script>	
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
</body>
</html>