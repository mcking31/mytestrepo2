<div class="alert-group">
	<div class="alert success hidden">
		<div class="content">
			<p class="title">
				Prefunding Completed
			</p>
			<p class="description">
				An amount of <b>P 200,000.00</b> is credited to <b>Ascend Ecommerce Philippines</b> on 07-06-2016 11:00 AM
			</p>
		</div>
		<i class="icon fa fa-times close"></i>
	</div>
	<div class="alert error hidden">
		<div class="content">
			<p class="title">
				Prefunding Failed
			</p>
			<p class="description">
				Please try again
			</p>
		</div>
		<i class="icon fa fa-times close"></i>
	</div>
</div>
<div class="breadcrumb">
	<div class="breadcrumb-group">
		<div class="active section">Prefunding</div>
	</div>
</div>
<div class="box">
	<div class="box-title">
		<h3>
			Prefunding
		</h3>
	</div>
	<div class="box-content mrg-top-30">
		<div class="numbered-group">
			<div class="numbered-form">
				<div class="description">
					<div class="form label-set dropdown-form">
						<label for="company-name">Company Name</label>
						<?php 
							echo form_dropdown( 
								'company-name',
								$companies,
								'',
								"name='company-name' id='company-name' class='field ui single selection dropdown mirror-input' data-mirror='#mirrorCompany'"
							);
						?>
						<span class="note"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="numbered-group">
			<div class="numbered-form">
				<div class="description">
					<div class="form label-set amount-form">
						<label for="amount">Enter Amount</label>
						<span class="currency-icon">&#8369;</span>
						<input type="text" class="field mirror-input-amount" data-mirror="#mirrorAmount" id="amount" placeholder="0.00">
						<span class="note"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="numbered-group">
			<div class="numbered-form">
				<div class="number">
					&nbsp;
				</div>
				<div class="description right-align">
					<div class="form">
						<button class="button orange fat modal-data-trigger" data-modal="#prefund" id="prefundSubmit">
							Prefund
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="ui modal tiny" id="prefund">
	<div class="content">
		<div class="alert-group hidden">
			
		</div>
		<h3 class="center-align">
			Confirm Prefunding
		</h3>
		<div class="mrg-top-20">
			<div class="mrg-top-10">
				<div class="box gray">
					<table class="table basic">
						<tbody>
							<tr>
								<td width="40%" class="bold">Company Name: </td>
								<td><span id="mirrorCompany"></span></td>
							</tr>
							<tr class="orange">
								<td width="40%" class="bold">Prefund Amount: </td>
								<td class="bold orange-color">P <span id="mirrorAmount"></span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="confirm check">
					<div class="confirm-checkbox">
						<div class="ui checkbox">
						  <input type="checkbox" name="confirm" id="confirmPrefund">
						  <label>&nbsp;</label>
						</div>
					</div>
					<div class="confirm-desc">
						I confirm that the information provided is accurate. <br>
						Clicking <b>CONFIRM</b> will prefund the entered amount.
					</div>
				</div>
				<div class="response center-align">
					<button class="button gray close">
						Cancel
					</button>
					<button class="button orange loading-after" data-loading-label="Processing" disabled id="confirm">
						Confirm
					</button>
				</div>
			</div>
		</div>
	</div>
</div>