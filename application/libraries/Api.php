<?php 

require_once APPPATH . 'vendor/autoload.php';

class Api{
	
	public $api_server;
	public $path;
	public $credentials;
	
	public function __construct( $args = [] ){
		$this->api_server = new GuzzleHttp\Client(['verify' => FALSE]);
	}
	
	public function call_api( $api_method, $args = [] ){
		$result = [];
		
		try{
			
			$params = ENV['core']['credentials'];
			$params['json'] = [ 
				'args' => $args
			];
					
			$res = $this->api_server->request( 
				'POST',
				ENV['core']['path'] . $api_method,
				$params
			);
			
			$_response = $res->getBody()->getContents();
			
			$result = json_decode( $_response, TRUE );
			
		} catch (SoapFault $fault){
			
		} catch (EngineException $e){
			
		}
		
		return $result;
	}
	
	public function __call( $method, $args ){
		$response = [];
		
		try{
			$args = $args[0] ?? [];
			$var = str_replace(strtolower(substr ( $method , 0, 4 )), '', $method);
			switch( strtolower(substr ( $method , 0, 4 )) ):
				case 'set_':
					if( isset( $this->$var ) )
						$this->$var = $args;
				break;
				case 'get_':
					if( isset( $this->$var ) )
						return ( $args == 'json' ) ? json_encode( $this->$var ) : $this->$var;
				break;
				case 'add_':
					if( isset( $this->$var ) )
						$this->$var = array_push( $this->$var, $args );
				break;
				default:
					if( function_exists( $method ) ):
						$response = $this->method( isset( $args ) ?? [] );
					else:
						$response = $this->call_api( $method, $args );
					endif;
				break;
			endswitch;
		} catch ( Throwable $e ){
			
		}
		
		return $response;
	}
}
