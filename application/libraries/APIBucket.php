<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*

HOW TO USE:

---------------------------
$api_bucket = new APIBucket;

$params = [];

$api_bucket->register(
	$params,
	[
		'baseUrl' => 'https://journey.globe.com.ph/rs/index.php/',
		'apiMethod' => 'get',
		'accessId' => 'admin',
		'accessPass' => '1234',
		'extHeaders' => [],
		'accessToken' => ['HAY' => 'token'],
		'module' => 'core',
		'action' => 'profile'
	]
);

module 		= core 
controller	= register
method		= profile

or call register/profile api with $api_bucket->core__register__profile

---------------------------
extends APIBucket

$this->set_base_url( 'https://journey.globe.com.ph/rs/index.php/' );
$this->set_method( 'DELETE' );
$this->set_auth( 'admin', '1234' );
$this->set_token( ['HAY' => 'token'] );
$this->set_headers( ['Content-Type' => 'application/json'] );
$this->register( $params );

*/

class APIBucket extends APIBucketCore{
	
	private $access_config = [];
	private $headers = [];
	private $access_id = NULL;
	private $access_pass = NULL;
	private $access_token = NULL;
	private $method = 'POST';
	private $module = '';
	private $controller_method = '';
	private $url = '';
	public $async = FALSE;
	
	public function __construct( $ext_config = [] ){
		parent::__construct();
		$this->access_config = array_merge( $ext_config, $this->access_config );
	}
	
	# DESC:  to append to base_url
	# PARAM: core/ or core 
	public function set_module( $url = '' ){
		if( 
			!empty( $url )
			&&
			$url[ strlen($url) - 1 ] != '/'
		) $url .= '/';
		$this->module = $url;
	}
	
	# DESC:  to append to controller
	# PARAM: process/ or process
	public function set_action( $url = '' ){
		// if( 
			// !empty( $url )
			// &&
			// $url[ strlen($url) - 1 ] != '/'
		// ) $url .= '/';
		$this->controller_method = $url;
	}
	
	# DESC:  set authentication details
	# PARAM: admin, 1234
	public function set_auth( $access_id = '', $access_pass = '' ){
		$this->access_id = $access_id;
		$this->access_pass = $access_pass;
	}
	
	# DESC:  set access name and access token
	# PARAM TYPE: array
	# PARAM SAMPLE: ['DBCORE-API-KEY', '35a7f8a177e7d59e256dd8d153b824507101cb950f91aa86d9a7413243b2c324']
	public function set_token( $token = [] ){
		$this->access_token = $token;
		$this->headers = array_merge( $token, $this->headers );
	}
	
	# DESC:  set headers
	# PARAM TYPE: array
	# PARAM SAMPLE: ['Content-Type' => 'application/json']
	public function set_headers( $headers = [] ){
		$this->headers = array_merge( $headers, $this->headers );
	}
	
	# DESC: set HTTP method
	# PARAM: 'PUT' 'POST' 'DELETE' 'GET'
	public function set_method( $method ){
		$method = strtolower( $method );
		
		switch( $method ){
			case 'post': case 'get': case 'put': case 'delete':
				$this->method = $method;
			break;
			default:
				$this->method = 'POST';
			break;
		}
	}
	
	public function set_base_url( $url ){
		$this->url = $url;
	}
	
	protected function get_auth(){
		return ( !empty( $this->access_id ) && !empty( $this->access_pass ) ) ? [ $this->access_id, $this->access_pass ] : NULL;
	}
	
	protected function get_method(){
		return $this->method;
	}
	
	protected function get_headers(){
		return $this->headers;
	}
	
	protected function get_token(){
		return $this->access_token;
	}
	
	protected function get_module(){
		return $this->module;
	}
	
	protected function get_action(){
		return $this->controller_method;
	}	

	protected function get_base_url(){
		return $this->url;
	}
}